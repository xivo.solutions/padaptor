package controllers

import javax.inject._

import play.api.Logger
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json.Json
import play.api.libs.ws.{WSClient, WSRequest}

import scala.concurrent.duration._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class AdapterController @Inject()(ws: WSClient, configuration: play.api.Configuration) extends Controller {
  val log = Logger(getClass.getName)

  case class CallData(command: String, user_name: String, text: String, channel_name: String)


  val callDataForm = Form(
    mapping(
      "command" -> text,
      "user_name" -> text,
      "text" -> text,
      "channel_name" -> text
    )(CallData.apply)(CallData.unapply)
  )

  val host = configuration.getString("xuc.url").getOrElse("locahost")
  val urlDial = s"https://$host/xuc/api/1.0/dial/avencall.com/"
  val urlDialByName = s"https://$host/xuc/api/1.0/dialByUsername/avencall.com/"


  val responseData = Json.obj(
    "response_type" -> "ephemeral"
  )

  def call = Action { implicit request =>
    callDataForm.bindFromRequest.fold(
      formWithErrors => {
        log.error(s"invalid call data $formWithErrors")
        BadRequest(s"invalid call data $formWithErrors")
      },
      callData => {
        log.info(s"calling $callData")

        val (userUrl, wsData) = if (callData.text.startsWith("@")) {
          (s"$urlDialByName${callData.user_name}/", Json.obj("username" -> callData.text.replace("@","")))
        } else {
          if(callData.text.isEmpty) (s"$urlDialByName${callData.user_name}/", Json.obj("username" -> callData.channel_name))
          else  (s"$urlDial${callData.user_name}/", Json.obj("number" -> callData.text))
        }

        log.info(userUrl)

        val request: WSRequest = ws.url(userUrl)

        val complexRequest: WSRequest = request.withHeaders("Content-Type" -> "application/json")
          .withRequestTimeout(10000.millis)

        complexRequest.post(wsData)

        log.info(s"called $callData : $userUrl $wsData")

        Ok(responseData ++ Json.obj("username" -> callData.user_name, "text" -> s"${callData.text} called"))
      }
    )
  }

}
