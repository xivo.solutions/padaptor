#!/usr/bin/env bash
set -e

echo building version $1 tag $2

sbt clean test docker:publishLocal
docker tag xivoxc/padaptor:$1 xivoxc/padaptor:$2
docker push xivoxc/padaptor:$1
docker push xivoxc/padaptor:$2
