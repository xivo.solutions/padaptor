
name := """padaptor"""

version := "1.1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala).enablePlugins(DockerPlugin)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)


maintainer in Docker := "Jean-Yves LEBLEU <jylebleu@avencall.com>"

dockerBaseImage := "azul/zulu-openjdk:8u66"

dockerExposedPorts := Seq(9000)

dockerExposedVolumes := Seq("/conf")

dockerRepository := Some("xivoxc")

dockerEntrypoint := Seq("bin/padaptor_docker")
